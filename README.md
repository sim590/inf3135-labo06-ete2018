# INF3135, laboratoire 06 - Solution

Voir le sous-module `array`.

## Initialisation

### Sur Gitlab

Il n'y a qu'à cliquer sur le lien du répertoire.

### Sur sa propre machine

Vous devez initialiser le sous-modules comme suit:

```sh
$ git submodule update --init
```

Ensuite, le dépôt `array` sera disponible dans le sous-répertoire `array`.

<!-- vim: set ts=4 sw=4 tw=80 et :-->

